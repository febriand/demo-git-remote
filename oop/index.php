<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep=new animal("Shaun");
echo "Name : ".$sheep->nama ."<br>";
echo "Leg : ".$sheep->leg ."<br>"; 
echo "Cold_Blooded : ".$sheep->cold_blooded ."<br><br>";

$kodok=new frog("buduk");
echo "Name : ".$kodok->nama ."<br>";
echo "Leg : ".$kodok->leg ."<br>"; 
echo "Cold_Blooded : ".$kodok->cold_blooded ."<br>";
echo $kodok->jump()."<br><br>";

$sungokong=new ape("kera sakti");
echo "Name : ".$sungokong->nama ."<br>";
echo "Leg : ".$sungokong->leg ."<br>"; 
echo "Cold_Blooded : ".$sungokong->cold_blooded ."<br>";
echo $sungokong->yell()."<br><br>";

?>